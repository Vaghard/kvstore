//
//  Constants.swift
//  KVOStore
//
//  Created by Hardik on 18/12/16.
//  Copyright © 2016 Hardik. All rights reserved.
//

import Foundation


struct Constants
{
    //Constants for JSON error codes
    struct JSONErrorMessages {
        static let JSON_SERIALIZATION_ERROR = "Failed to create JSON."
    }
    
    //Constants for func return values
    struct MethodReturnValues {
        static let STATUS_SUCCESS = 0;
        static let STATUS_FAILURE = 1;
    }
}
