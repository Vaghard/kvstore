//
//  DataProvider.swift
//  KVOStore
//
//  Created by Hardik on 18/12/16.
//  Copyright © 2016 Hardik. All rights reserved.
//

import Foundation
import UIKit

typealias CellConfigurationBlock = (_ item: AnyObject, _ cell: KeyValueCell) -> Void

class DataProvider: NSObject {
    
    var items = [AnyObject]()
    var cellIdentifier: String?
    var configurationBlock: CellConfigurationBlock?
    
    init(items: [AnyObject], cellIdentifier identifier: String, configurationBlock block: @escaping CellConfigurationBlock) {
        self.items = items
        self.cellIdentifier = identifier
        self.configurationBlock = block
    }
}

extension DataProvider: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier!, for: indexPath) as! KeyValueCell
        let item = items[indexPath.row]
        configurationBlock?(item, cell)
        return cell
    }
    
    
}
