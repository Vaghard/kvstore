//
//  Company.swift
//  KVOStore
//
//  Created by Hardik on 21/12/16.
//  Copyright © 2016 Hardik. All rights reserved.
//

import Foundation

class Person: KVStorable {
    
    let firstName: String
    let lastName: String
    let gender: String
    let email: String
    let id: String
    
    //Create table statement for KVStore_Text table
    static var createStatement: String {
        return "CREATE TABLE if not exists \(SQLiteTable.customClasssTableName)(" +
            "\(SQLiteTable.customClassTableKeyID) TEXT PRIMARY KEY NOT NULL," +
            "\(SQLiteTable.customClassTableKeyFirstName) TEXT," +
            "\(SQLiteTable.customClassTableKeyLastName) TEXT," +
            "\(SQLiteTable.customClassTableKeyEmail) TEXT," +
            "\(SQLiteTable.customClassTableKeyGender) TEXT" +
        ");"
    }
    
    init(id: String, firstName: String, lastName: String, gender: String, email:String) {
        self.id = id
        self.firstName = firstName;
        self.lastName = lastName;
        self.gender = gender;
        self.email = email;
    }
    
    func dictionaryRepresentation() -> [String : AnyObject]? {
        var dictionary = [String:AnyObject]();
        dictionary["id"] = self.id as AnyObject?
        dictionary["firstName"] = self.firstName as AnyObject?
        dictionary["lastName"] = self.lastName as AnyObject?
        dictionary["gender"] = self.gender as AnyObject?
        dictionary["email"] = self.email as AnyObject?
        
        return ["key": dictionary as AnyObject]
    }
    
    class func personObjectFromDictionary(dictionary: [String : AnyObject]) -> Person? {
        if let firstName = dictionary["firstName"] as? String,
            let lastName = dictionary["lastName"] as? String,
            let gender = dictionary["gender"] as? String,
            let email = dictionary["email"] as? String,
            let id = dictionary["id"] as? String {
        
            return Person(id: id, firstName: firstName, lastName: lastName, gender: gender, email: email)
        }
        return nil
    }
}
