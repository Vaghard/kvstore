//
//  SQLiteStore.swift
//  KVOStore
//
//  Created by Hardik on 18/12/16.
//  Copyright © 2016 Hardik. All rights reserved.
//

import Foundation

public enum SQLiteKVStoreValueType: Int32 {
    case normal
    case blob
    
    case integer
    case real
    case text
    case unknown
}

class KVStore {
    var storeName : String;
    
    init(name: String) {
        storeName = name;
    }
    
    let kvStoreCoordinator : SQLiteKVStoreCoordinator = SQLiteKVStoreCoordinator.sharedInstance;
    
    func put(key: String, value: AnyObject) throws -> Int {
        var dictionary: [String:AnyObject]?
        var returnValue = Constants.MethodReturnValues.STATUS_FAILURE;
        
        var valueTypeTest = SQLiteKVStoreValueType.unknown
        if value is Double {
            dictionary = (value as! Double).dictionaryRepresentation()
            valueTypeTest = .real
        } else if value is Int {
            dictionary = (value as! Int).dictionaryRepresentation()
            valueTypeTest = .integer
        } else if value is String {
            dictionary = (value as! String).dictionaryRepresentation()
            valueTypeTest = .text
        } else if value is Array<AnyObject> {
            dictionary = (value as! Array<AnyObject>).dictionaryRepresentation()
            valueTypeTest = .blob
        } else if value is [String:AnyObject] {
            dictionary = ["key":(value as! [String:AnyObject] as AnyObject)]
            valueTypeTest = .blob
        } else if value is Date {
            let timestamp = (value as! Date).timeIntervalSince1970
            dictionary = timestamp.dictionaryRepresentation()
            valueTypeTest = .real
        } else if value is KVStorable {
            dictionary = (value as! KVStorable).dictionaryRepresentation()
            valueTypeTest = .blob
        } else {
            throw KVStoreWriteObjectError.unsupportedValue(message: "Provided value type is not supported for now.");
        }
        
        guard let dictValue = dictionary else {
            throw KVStoreWriteObjectError.dictionaryConversionFailed(message: "No dictionary representation available");
        }
        
        //Serialize NSDictionary to JSON
        let jsonData = try JSONSerialization.data(withJSONObject: dictValue, options: .prettyPrinted);
        let jsonString = String(data: jsonData, encoding: String.Encoding.utf8) ?? "{}"
        let kvPair : KVStoreGenericTabel = KVStoreGenericTabel(key: key, stringValue: jsonString, valueType: valueTypeTest, storeName: self.storeName)
        
        do {
            //Synchronize access to DB
            objc_sync_enter(self)
            try kvStoreCoordinator.db?.insertGenericValue(kvPair: kvPair);
            objc_sync_exit(self);
            //Update return status as Success
            return Constants.MethodReturnValues.STATUS_SUCCESS
        } catch SQLiteError.bind(let message) {
            print(message);
            throw SQLiteError.bind(message: message)
        } catch SQLiteError.step(let message) {
            print(message);
            throw SQLiteError.step(message: message)
        } catch {
            print(Constants.JSONErrorMessages.JSON_SERIALIZATION_ERROR);
        }
        return returnValue;
    }
    
    func getGenericValue(forKey key: String) -> KVStoreGenericTabel? {
        do {
            //Synchronize access to DB
            objc_sync_enter(self)
            let kvPair = try kvStoreCoordinator.db?.getGenericValue(forKey: key, storeName: self.storeName)
            objc_sync_exit(self);
            return kvPair
        } catch {
            print(error)
        }
        return nil
    }
    
}
