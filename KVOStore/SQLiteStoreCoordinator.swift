//
//  SQLiteStoreCoordinator.swift
//  KVOStore
//
//  Created by Hardik on 18/12/16.
//  Copyright © 2016 Hardik. All rights reserved.
//

import Foundation

class SQLiteKVStoreCoordinator {
    static let sharedInstance = SQLiteKVStoreCoordinator();
    var db: SQLiteDatabase?
    
    private init() {
        do {
            let docPath = FileManager.default.urls(for: .documentDirectory, in: .allDomainsMask).first
            let storePath = docPath?.appendingPathComponent("KeyValueStore.sqlite").path
            print("DB file path : \(storePath)")
            //Open DB once while instantiating this store co-ordinator
            db = try SQLiteDatabase.open(dbPath: storePath!);
            try db?.createTable(table: KVStoreGenericTabel.self)
            try db?.createTable(table: Person.self)

        } catch SQLiteError.createDatabase(let message) {
            print("Unable to create database. Verify that you have provided correct path.");
            print(message);
        } catch SQLiteError.openDatabase(let message) {
            print("Unable to open database. Verify that you created the databse.");
            print(message);
        } catch SQLiteError.step(let message) {
            print(message);
        }  catch {
            print("Unknown error");
        }
    }
}
