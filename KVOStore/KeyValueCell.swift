//
//  KeyValueCell.swift
//  KVOStore
//
//  Created by Hardik on 18/12/16.
//  Copyright © 2016 Hardik. All rights reserved.
//

import UIKit

class KeyValueCell: UITableViewCell {

    func configure(withItem item: AnyObject) {
        self.textLabel?.text = item as? String
    }

}
