//
//  Store.swift
//  KVOStore
//
//  Created by Hardik on 18/12/16.
//  Copyright © 2016 Hardik. All rights reserved.
//

import Foundation

//Constants for SQLite table name, columns
struct SQLiteTable {
    static let genericTableName = "KVStore_generic"
    static let customClasssTableName = "customClassTable"
    static let columnNameKey = "Key"
    static let columnNameValue = "Value"
    static let columnNameValueType = "Value_type"
    static let storeName = "Store_Name"
    static let customClassTableKeyFirstName = "firstName"
    static let customClassTableKeyLastName = "lastName"
    static let customClassTableKeyGender = "gender"
    static let customClassTableKeyEmail = "email"
    static let customClassTableKeyID = "id"
}

//Constants for custom sqlite error codes
struct SQLiteErrorMessages {
    static let dbCreationFailed = "Failed to create database."
    static let dbOpenFailed = "Failed to open database."
    static let prepareStatmentFailed = "Failed to prepare statement."
    static let stateStatementFailed = "Failed to execute prepared statement."
    static let bindValueFailed = "Failed to bind values to prepared statement."
    static let failedToRead = "Failed to read data from Tabel."
    static let defaultError = "No error message provided from sqlite."
}

enum SQLiteError : Error {
    case createDatabase(message: String)
    case openDatabase(message: String)
    case prepare(message: String)
    case step(message: String)
    case bind(message: String)
    case failedToRead(message: String)
}

public struct KVStoreGenericTabel {
    //Key will always be string
    public let key: String
    
    //Value will always be JSON string
    public let stringValue: String
    
    public var value: AnyObject? {
        do {
            if let valueData = stringValue.data(using: .utf8) {
                let valueDictionary = try JSONSerialization.jsonObject(with: valueData, options: .allowFragments) as AnyObject
                if let valueObject = valueDictionary["key"] as? AnyObject {
                    return valueObject
                }
            }
            
        } catch {
            print("Error deserialization : \(error)")
        }
        return nil

    }
    
    //Type of stored value/object
    public let valueType: SQLiteKVStoreValueType
    
    //Store name
    public let storeName: String
    
    //Create table statement for KVStore_Text table
    static var createStatement: String {
        return "CREATE TABLE if not exists \(SQLiteTable.genericTableName)(" +
            "\(SQLiteTable.columnNameKey) TEXT PRIMARY KEY NOT NULL," +
            "\(SQLiteTable.columnNameValue) BLOB," +
            "\(SQLiteTable.columnNameValueType) INTEGER NOT NULL," +
            "\(SQLiteTable.storeName) TEXT NOT NULL" +
        ");"
    }
}


class SQLiteDatabase {
    let dbPointer: OpaquePointer;
    
    private init(dbPointer: OpaquePointer) {
        self.dbPointer = dbPointer;
    }
    
    /**
     Close the database before the memory for the class instance is deallocated
     */
    deinit {
        sqlite3_close(self.dbPointer)
    }
    
    // Open or Create Database
    @discardableResult
    static func open(dbPath: String) throws -> SQLiteDatabase {
        var db: OpaquePointer? = nil
        
        let fileManager = FileManager.default
        if !fileManager.fileExists(atPath: dbPath) {
            guard fileManager.createFile(atPath: dbPath, contents: nil, attributes: nil) else {
                print(SQLiteErrorMessages.dbCreationFailed);
                throw SQLiteError.createDatabase(message: SQLiteErrorMessages.dbCreationFailed);
            }
        }
        
        if sqlite3_open(dbPath, &db) == SQLITE_OK {
            return SQLiteDatabase(dbPointer: db!);
        }
        else {
            defer {
                if db != nil {
                    sqlite3_close(db);
                }
            }
        }
        
        if let message = String.init(validatingUTF8: sqlite3_errmsg(db)) {
            print(SQLiteErrorMessages.dbOpenFailed);
            throw SQLiteError.openDatabase(message: message);
        }  else {
            print(SQLiteErrorMessages.defaultError);
            throw SQLiteError.openDatabase(message: SQLiteErrorMessages.defaultError);
        }
    }
    
    //Error message property
    var errorMessage: String {
        if let errorMessage = String.init(validatingUTF8: sqlite3_errmsg(dbPointer)) {
            return errorMessage;
        } else {
            return SQLiteErrorMessages.defaultError;
        }
    }
    
    var errorCode: Int32 {
        return sqlite3_errcode(dbPointer)
    }
}


extension SQLiteDatabase {

    // Convenience method to create prepared statement for any of the SQLite operations
    func prepareStatement(sql: String) throws -> OpaquePointer {
        var statement: OpaquePointer? = nil
        guard sqlite3_prepare_v2(self.dbPointer, sql, -1, &statement, nil) == SQLITE_OK else {
            print(SQLiteErrorMessages.prepareStatmentFailed);
            throw SQLiteError.prepare(message: errorMessage);
        }
        
        return statement!
    }


    //MARK: Create table for array value
    func createTable(table: KVStoreGenericTabel.Type) throws {
        let createTableStatement = try prepareStatement(sql: table.createStatement);
        defer {
            sqlite3_finalize(createTableStatement);
        }
        
        guard sqlite3_step(createTableStatement) == SQLITE_DONE else {
            print(SQLiteErrorMessages.stateStatementFailed);
            throw SQLiteError.step(message: errorMessage);
        }
        
        print("\(table) Table created successfully - Text.")
    }
    
    func createTable(table: Person.Type) throws {
        let createTableStatement = try prepareStatement(sql: table.createStatement);
        defer {
            sqlite3_finalize(createTableStatement);
        }
        
        guard sqlite3_step(createTableStatement) == SQLITE_DONE else {
            print(SQLiteErrorMessages.stateStatementFailed);
            throw SQLiteError.step(message: errorMessage);
        }
        
        print("\(table) Table created successfully - Text.")
    }
    
    func insertGenericValue(kvPair: KVStoreGenericTabel) throws {
        let insertSql = "INSERT INTO \(SQLiteTable.genericTableName) (\(SQLiteTable.columnNameKey), \(SQLiteTable.columnNameValue), \(SQLiteTable.columnNameValueType), \(SQLiteTable.storeName)) VALUES (?, ?, ?, ?);"
        let insertStatement = try prepareStatement(sql: insertSql)
        defer {
            sqlite3_finalize(insertStatement);
        }
        
        let key: String = kvPair.key;
        let strValue: String? = kvPair.stringValue;
        let value = strValue?.data(using: String.Encoding.utf8) as NSData?
        let valueType = kvPair.valueType.rawValue
        let storeName: String = kvPair.storeName;
        
        if let data = value  {
            let SQLITE_TRANSIENT = unsafeBitCast(-1, to: sqlite3_destructor_type.self);
            guard sqlite3_bind_text(insertStatement, 1, (key as NSString).utf8String, -1, nil) == SQLITE_OK  &&
                sqlite3_bind_blob(insertStatement, 2, data.bytes, Int32(data.length), SQLITE_TRANSIENT) == SQLITE_OK &&
                sqlite3_bind_int(insertStatement, 3, valueType) == SQLITE_OK &&
                sqlite3_bind_text(insertStatement, 4, (storeName as NSString).utf8String, -1, nil) == SQLITE_OK else {
                    print(SQLiteErrorMessages.bindValueFailed)
                    throw SQLiteError.bind(message: errorMessage)
            }
            
            guard sqlite3_step(insertStatement) == SQLITE_DONE else {
                print(SQLiteErrorMessages.stateStatementFailed)
                
                if errorCode == 19 { // if object/value already available for key
                    try! updateGenericValue(kvPair: kvPair)
                    return
                }
                
                throw SQLiteError.step(message: errorMessage)
            }
            
            print("Inserted tuple successfully - Text.")
        }
    }
    
    
    
    func updateGenericValue(kvPair: KVStoreGenericTabel) throws {
        let insertSql = "UPDATE \(SQLiteTable.genericTableName) SET \(SQLiteTable.columnNameValue) = ? WHERE \(SQLiteTable.columnNameKey) = ?;"
        let insertStatement = try prepareStatement(sql: insertSql)
        defer {
            sqlite3_finalize(insertStatement);
        }
        
        let key: String = kvPair.key;
        let strValue: String? = kvPair.stringValue;
        let value = strValue?.data(using: String.Encoding.utf8) as NSData?
        
        if let data = value  {
            let SQLITE_TRANSIENT = unsafeBitCast(-1, to: sqlite3_destructor_type.self)
            guard sqlite3_bind_blob(insertStatement, 1, data.bytes, Int32(data.length), SQLITE_TRANSIENT) == SQLITE_OK &&
                sqlite3_bind_text(insertStatement, 2, (key as NSString).utf8String, -1, nil) == SQLITE_OK else {
                    print(SQLiteErrorMessages.bindValueFailed)
                    throw SQLiteError.bind(message: errorMessage)
            }
            
            guard sqlite3_step(insertStatement) == SQLITE_DONE else {
                print(SQLiteErrorMessages.stateStatementFailed);
                throw SQLiteError.step(message: errorMessage);
            }
            
            print("Successfully updated row.")
        }
    }
    
    //Read key value pair for given key
    @discardableResult
    func getGenericValue(forKey key: String, storeName: String) throws -> KVStoreGenericTabel? {
        let querySql = "SELECT * FROM \(SQLiteTable.genericTableName) WHERE \(SQLiteTable.columnNameKey) = ? AND \(SQLiteTable.storeName) = ?;"
        guard let queryStatement = try? prepareStatement(sql: querySql) else  {
            print(SQLiteErrorMessages.prepareStatmentFailed)
            return nil;
        }
        
        defer {
            sqlite3_finalize(queryStatement);
        }
        
        guard sqlite3_bind_text(queryStatement, 1, (key as NSString).utf8String, -1, nil) == SQLITE_OK &&
            sqlite3_bind_text(queryStatement, 2, (storeName as NSString).utf8String, -1, nil) == SQLITE_OK else {
                print(SQLiteErrorMessages.bindValueFailed)
                throw SQLiteError.bind(message: errorMessage)
        }
        
        guard sqlite3_step(queryStatement) == SQLITE_ROW else {
            print(SQLiteErrorMessages.stateStatementFailed)
            throw SQLiteError.step(message: errorMessage)
        }
        
        let valueType = sqlite3_column_int(queryStatement, 2)
        if let valueTypeEnum = SQLiteKVStoreValueType(rawValue: valueType) {
            let column1Value = sqlite3_column_text(queryStatement, 0);
            let key = String(cString: column1Value!)
            
            let len = sqlite3_column_bytes(queryStatement, 1)
            guard let bytes = sqlite3_column_blob(queryStatement, 1) else {
                throw SQLiteError.failedToRead(message: SQLiteErrorMessages.failedToRead)
            }
            
            let valueData = NSData(bytes: bytes, length: Int(len))
            let valueString = String(data: valueData as Data, encoding: .utf8)!
            
            let storeNameColumnValue = sqlite3_column_text(queryStatement, 2)
            let storeName = String(cString: storeNameColumnValue!)
            
            return KVStoreGenericTabel(key: key, stringValue: valueString, valueType: valueTypeEnum, storeName: storeName);
        }
        return nil
    }
    
    
    
    
    
    //MARK: ANother Table for saving Custom object
    
    // Insert custom object to DB
    func insertCustomClassValue(customObject: Person) throws {
        let insertSql = "INSERT INTO \(SQLiteTable.customClasssTableName) (\(SQLiteTable.customClassTableKeyID), \(SQLiteTable.customClassTableKeyFirstName), \(SQLiteTable.customClassTableKeyLastName), \(SQLiteTable.customClassTableKeyEmail), \(SQLiteTable.customClassTableKeyGender)) VALUES (?, ?, ?, ?, ?);"
        let insertStatement = try prepareStatement(sql: insertSql)
        defer {
            sqlite3_finalize(insertStatement);
        }
        
        guard sqlite3_bind_text(insertStatement, 1, (customObject.id as NSString).utf8String, -1, nil) == SQLITE_OK  &&
            sqlite3_bind_text(insertStatement, 2, (customObject.firstName as NSString).utf8String, -1, nil) == SQLITE_OK  &&
            sqlite3_bind_text(insertStatement, 3, (customObject.lastName as NSString).utf8String, -1, nil) == SQLITE_OK  &&
            sqlite3_bind_text(insertStatement, 4, (customObject.email as NSString).utf8String, -1, nil) == SQLITE_OK  &&
            sqlite3_bind_text(insertStatement, 5, (customObject.gender as NSString).utf8String, -1, nil) == SQLITE_OK else {
                print(SQLiteErrorMessages.bindValueFailed)
                throw SQLiteError.bind(message: errorMessage)
        }
        
        guard sqlite3_step(insertStatement) == SQLITE_DONE else {
            print(SQLiteErrorMessages.stateStatementFailed);
            if errorCode == 19 { // if object/value already available for key
                do {
                    try updateCustomObject(customObject: customObject)
                } catch {
                    print("Error :\(error)")
                }
                return
            }

            throw SQLiteError.step(message: errorMessage);
        }
        
        print("Inserted tuple successfully - Text.")
    }
    
    //Update custom object to DB
    func updateCustomObject(customObject: Person) throws {
        let insertSql = "UPDATE \(SQLiteTable.customClasssTableName) SET \(SQLiteTable.customClassTableKeyFirstName) = ?, \(SQLiteTable.customClassTableKeyLastName) = ?, \(SQLiteTable.customClassTableKeyEmail) = ?, \(SQLiteTable.customClassTableKeyGender) = ? WHERE \(SQLiteTable.customClassTableKeyID) = ?;"
        let insertStatement = try prepareStatement(sql: insertSql)
        defer {
            sqlite3_finalize(insertStatement);
        }
        
        guard sqlite3_bind_text(insertStatement, 5, (customObject.id as NSString).utf8String, -1, nil) == SQLITE_OK  &&
            sqlite3_bind_text(insertStatement, 1, (customObject.firstName as NSString).utf8String, -1, nil) == SQLITE_OK  &&
            sqlite3_bind_text(insertStatement, 2, (customObject.lastName as NSString).utf8String, -1, nil) == SQLITE_OK  &&
            sqlite3_bind_text(insertStatement, 3, (customObject.email as NSString).utf8String, -1, nil) == SQLITE_OK  &&
            sqlite3_bind_text(insertStatement, 4, (customObject.gender as NSString).utf8String, -1, nil) == SQLITE_OK else {
                print(SQLiteErrorMessages.bindValueFailed)
                throw SQLiteError.bind(message: errorMessage)
        }
        
        guard sqlite3_step(insertStatement) == SQLITE_DONE else {
            print(SQLiteErrorMessages.stateStatementFailed);
            throw SQLiteError.step(message: errorMessage);
        }
        
        print("Successfully updated row.")
    }
    
    //Read custom object from db
    @discardableResult
    func getCustomObject(forId id: String) throws -> Person? {
        let querySql = "SELECT * FROM \(SQLiteTable.customClasssTableName) WHERE \(SQLiteTable.customClassTableKeyID) = ?;"
        guard let queryStatement = try? prepareStatement(sql: querySql) else  {
            print(SQLiteErrorMessages.prepareStatmentFailed)
            return nil;
        }
        
        defer {
            sqlite3_finalize(queryStatement);
        }
        
        guard sqlite3_bind_text(queryStatement, 1, (id as NSString).utf8String, -1, nil) == SQLITE_OK else {
                print(SQLiteErrorMessages.bindValueFailed)
                throw SQLiteError.bind(message: errorMessage)
        }
        
        guard sqlite3_step(queryStatement) == SQLITE_ROW else {
            print(SQLiteErrorMessages.stateStatementFailed)
            throw SQLiteError.step(message: errorMessage)
        }
        
        let firstName = sqlite3_column_text(queryStatement, 1)
        let lastName = sqlite3_column_text(queryStatement, 2)
        let email = sqlite3_column_text(queryStatement, 3)
        let gender = sqlite3_column_text(queryStatement, 4)

        
        if let fName = firstName,
            let lName = lastName,
            let emailId = email,
            let gendr = gender {
            
            let firstNameString = String(cString: fName)
            let lastNameString = String(cString: lName)
            let emailIdString = String(cString: emailId)
            let genderString = String(cString: gendr)
            return Person(id: id, firstName: firstNameString, lastName: lastNameString, gender: genderString, email: emailIdString)
        }
        return nil
    }

}
