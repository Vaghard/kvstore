//
//  ViewController.swift
//  KVOStore
//
//  Created by Hardik on 18/12/16.
//  Copyright © 2016 Hardik. All rights reserved.
//

import UIKit

class RootViewController: UITableViewController {

    var dataProvider: DataProvider?
    let kvStore = KVStore(name: "Store1");

    override func viewDidLoad() {
        super.viewDidLoad()
        //configureTableView()
    }
    
    func configureTableView() {
        dataProvider = DataProvider(items: ["Value1" as AnyObject], cellIdentifier: "KeyValueCell", configurationBlock: { (item, cell: KeyValueCell) in
            cell.configure(withItem: item)
        })
        tableView.dataSource = dataProvider
    }
    
    @IBAction func addValue(_ sender: Any) {
        let keyarray = "array"
        let keyDict = "dict"
        let keyInt = "int"
        let keyReal = "real"
        let keyText = "text"
        
        let valueArray = ["test1", "Test2", 1, 10.5] as [Any]
        let valueDict = ["key1": "value1", "key2": 100, "key3": 122.34] as [String : Any]
        let valueInt = 10000
        let valueReal = 1234.343
        let valueText = "Hello WOrld!"
        
        let customPersonObject = Person(id: "123",
                                        firstName: "Jai",
                                        lastName: "Viru",
                                        gender: "Male",
                                        email: "jaiviru@thakur.com")
        
        let actionSheet = UIAlertController(title: "Select value type to insert into DB", message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Int", style: .default, handler: {[weak self] (action) in
            self?.insertValue(valueInt  as AnyObject, forKey: keyInt)
        }))
        actionSheet.addAction(UIAlertAction(title: "Real", style: .default, handler: {[weak self] (action) in
            self?.insertValue(valueReal as AnyObject, forKey: keyReal)
        }))
        actionSheet.addAction(UIAlertAction(title: "Text", style: .default, handler: {[weak self] (action) in
            self?.insertValue(valueText as AnyObject, forKey: keyText)
        }))
        actionSheet.addAction(UIAlertAction(title: "Array", style: .default, handler: { [weak self] (action) in
            self?.insertValue(valueArray as AnyObject, forKey: keyarray)
        }))
        actionSheet.addAction(UIAlertAction(title: "Dictionary", style: .default, handler: { [weak self] (action) in
            self?.insertValue(valueDict as AnyObject, forKey: keyDict)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Custom Object", style: .default, handler: { [weak self] (action) in
            self?.insertValue(customPersonObject as AnyObject, forKey: "Custom Obj")
        }))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func insertValue(_ value: AnyObject, forKey key: String) {
        do {
            try kvStore.put(key: key, value: value)
        }
        catch KVStoreWriteObjectError.dictionaryConversionFailed(let message)
        {
            print(message);
        }
        catch let error as NSError
        {
            print("Error: \(error.localizedDescription)");
        }
        
        let genericTable = kvStore.getGenericValue(forKey: key)
        
        print("\(genericTable?.value)")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

