//
//  
//  KVOStore
//
//  Created by Hardik on 18/12/16.
//  Copyright © 2016 Hardik. All rights reserved.
//

import Foundation

enum KVStoreWriteObjectError : Error {
    case unsupportedValue(message: String)
    case dictionaryConversionFailed(message: String)
}

protocol KVStorable
{
    //Create dictionary representation of the object
    func dictionaryRepresentation() -> [String : AnyObject]?;
}

extension Int: KVStorable {
    func dictionaryRepresentation() -> [String : AnyObject]? {
        let numberRepresenation = NSNumber(value: self);
        let dictionaryRepresenation: [String : AnyObject]?  = ["key":numberRepresenation];
        return dictionaryRepresenation;
    }
}

extension Double: KVStorable {
    func dictionaryRepresentation() -> [String : AnyObject]? {
        let numberRepresenation = NSNumber(value: self);
        let  dictionaryRepresenation: [String : AnyObject]?  = ["key":numberRepresenation];
        return dictionaryRepresenation;
    }
}

extension String: KVStorable {
    func dictionaryRepresentation() -> [String : AnyObject]? {
        let  dictionaryRepresenation: [String : AnyObject]?  = ["key":self as AnyObject];
        return dictionaryRepresenation;
    }
}

extension Array: KVStorable {
    func dictionaryRepresentation() -> [String : AnyObject]? {
        var dictionaryRepresenation : [String : AnyObject]?;
        if self.count > 0 {
            dictionaryRepresenation  = ["key":self as AnyObject]
        }
        return dictionaryRepresenation
    }
}

