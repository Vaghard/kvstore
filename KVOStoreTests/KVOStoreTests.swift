//
//  KVOStoreTests.swift
//  KVOStoreTests
//
//  Created by Hardik on 18/12/16.
//  Copyright © 2016 Hardik. All rights reserved.
//

import XCTest
@testable import KVOStore

class KVOStoreTests: XCTestCase {
    
    var kvStore: KVStore?
    var storeCoordinator: SQLiteKVStoreCoordinator?
    var docDirURL: URL?
    var storePath: String?
    
    
    override func setUp() {
        super.setUp()
        kvStore = KVStore(name: "UnitTest")
        storeCoordinator = SQLiteKVStoreCoordinator.sharedInstance;
        docDirURL = FileManager.default.urls(for: .documentDirectory, in: .allDomainsMask).first
        storePath = docDirURL?.appendingPathComponent("KeyValueStore.sqlite").path
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        docDirURL = nil
        storePath = nil
        storeCoordinator = nil
        kvStore = nil
    }
    
    func XCAssertErrorHandler(file: String = #file, line: UInt = #line, _ block: () throws -> ()) {
        var errorMessage = ""
        do {
            try block()
        } catch KVStoreWriteObjectError.unsupportedValue(let message) {
            errorMessage = message
        } catch KVStoreWriteObjectError.dictionaryConversionFailed(let message) {
            errorMessage = message
        } catch SQLiteError.bind(let message) {
            errorMessage = message
        } catch SQLiteError.step(let message) {
            errorMessage = message
        }  catch SQLiteError.openDatabase(let message) {
            errorMessage = message
        }  catch SQLiteError.prepare(let message) {
            errorMessage = message
        }  catch SQLiteError.failedToRead(let message) {
            errorMessage = message
        }  catch SQLiteError.createDatabase(let message) {
            errorMessage = message
        }  catch {
            errorMessage = error.localizedDescription
        }
        
        if !errorMessage.isEmpty {
            XCTFail(errorMessage, file: #file, line: line)
        }

    }
    
    //Test case to check whether DB is created at given path
    func testDBExistence()
    {
        guard let path = storePath else {
            XCTAssert(false, "Store path should not be nil")
            return
        }
        let status: Bool = FileManager.default.fileExists(atPath: path);
        XCTAssertTrue(status);
    }
    
    func testDBOpen() {
        guard let path = storePath else {
            XCTAssert(false, "Store path should not be nil")
            return
        }
        XCAssertErrorHandler {
            try SQLiteDatabase.open(dbPath: path)
        }
    }
    
    func testTableCreation() {
        XCAssertErrorHandler {
            try storeCoordinator?.db?.createTable(table: KVStoreGenericTabel.self)
        }
    }
    
    func testJSONSerializationForSingleString() {
        let value = "Hardik Vaghasia" as AnyObject;
        let dictionary : [String:AnyObject] = ["Key":value];
        XCAssertErrorHandler {
            try JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted)
        }
    }
    
    //Test to insert string value to DB
    func testInsertStringToDB() {
        let value = "Hardik Vaghasia" as AnyObject
        let dictionary = ["Key":value]
        var jsonData = Data()
        XCAssertErrorHandler {
            jsonData = try JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted)
        }
        guard let jsonString = String(data: jsonData, encoding: .utf8) else {
            XCTAssert(false, "json value should not be nil")
            return
        }
        let kvPair = KVStoreGenericTabel(key: "fullName1", stringValue: jsonString, valueType: .text, storeName: "Store")
        XCAssertErrorHandler {
            try storeCoordinator?.db?.insertGenericValue(kvPair: kvPair)
        }
    }
    
    //Test to insert string value to DB
    func testGetStringValueFromDB() {
        XCAssertErrorHandler {
            _ = try storeCoordinator?.db?.getGenericValue(forKey: "fullName1", storeName: "Store")
        }
    }
    
    //Test to insert Int value to DB
    func testInsertIntToDB() {
        let value = 1500 as AnyObject
        let dictionary = ["Key":value]
        var jsonData = Data()
        XCAssertErrorHandler {
            jsonData = try JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted)
        }
        guard let jsonString = String(data: jsonData, encoding: .utf8) else {
            XCTAssert(false, "json value should not be nil")
            return
        }
        let kvPair = KVStoreGenericTabel(key: "id", stringValue: jsonString, valueType: .integer, storeName: "Store")

        XCAssertErrorHandler {
            try storeCoordinator?.db?.insertGenericValue(kvPair: kvPair)
        }
    }
    
    //Test to insert float value to DB
    func testInsertFloatToDB() {
        let value = 111.11 as AnyObject
        let dictionary = ["Key":value]
        var jsonData = Data()
        XCAssertErrorHandler {
            jsonData = try JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted)
        }
        guard let jsonString = String(data: jsonData, encoding: .utf8) else {
            XCTAssert(false, "json value should not be nil")
            return
        }
        
        let kvPair = KVStoreGenericTabel(key: "balance", stringValue: jsonString, valueType: .real, storeName: "Store")
        
        XCAssertErrorHandler {
            try storeCoordinator?.db?.insertGenericValue(kvPair: kvPair)
        }
    }
    
    //Test case to check whether object can be inserted
    //Note: THis will fail as NSDate is not handled in code
    func testAddDateToStore() {
        let kvStore = KVStore(name: "StoreOne");
        XCAssertErrorHandler {
            try kvStore.put(key: "currentDate", value: NSDate());
        }
    }
    
    //Test case to check whether array key and array value can be inserted
    func testAddArrayToDB()
    {
        let key = "randomArray"
        var values = [AnyObject]()
        values.append("One" as AnyObject)
        values.append(2 as AnyObject)
        values.append(100.0 as AnyObject)
        values.append(["Key":"Value"] as AnyObject)

        var jsonData = Data()
        XCAssertErrorHandler {
            jsonData = try JSONSerialization.data(withJSONObject: values, options: .prettyPrinted)
        }
        guard let jsonString = String(data: jsonData, encoding: .utf8) else {
            XCTAssert(false, "json value should not be nil")
            return
        }
        
        let kvPair = KVStoreGenericTabel(key: key, stringValue: jsonString, valueType: .blob, storeName: "Store")
        
        XCAssertErrorHandler {
            try storeCoordinator?.db?.insertGenericValue(kvPair: kvPair);
        }
    }
    
    //Test to add and get custom object from database
    func testCustomObjectTransaction() {
        
        let mockPersonObject = Person(id: "100",
                                      firstName: "Basanti Jai Paji",
                                        lastName: "Basanti Viru Paji",
                                        gender: "Male",
                                        email: "jaiviru@thakur.com")
        let key = "personObject1"

        XCAssertErrorHandler {
            try kvStore?.put(key: key, value: mockPersonObject as AnyObject)
        }
        
        XCAssertErrorHandler {
            let objectValue = kvStore?.getGenericValue(forKey: key)
            guard let personObject = Person.personObjectFromDictionary(dictionary: objectValue?.value as! [String : AnyObject]) else {
                XCTAssert(false, "Could not create Person object from Dictionary read from DB.")
                return
            }
            
            XCTAssertEqual(personObject.id, mockPersonObject.id, "Person Id should b same.")
            XCTAssertEqual(personObject.firstName, mockPersonObject.firstName, "Person first name should b same.")
            XCTAssertEqual(personObject.lastName, mockPersonObject.lastName, "Person last name should b same.")
            XCTAssertEqual(personObject.gender, mockPersonObject.gender, "Person gender should b same.")
            XCTAssertEqual(personObject.email, mockPersonObject.email, "Person email should b same.")
        }
    }
    
    //Test to add and get custom object from database with New Table
    func testCustomObjectTransactionNewTable() {
        let mockPersonObject = Person(id: "100",
                                      firstName: "Jai Paji",
                                      lastName: "Viru Paji",
                                      gender: "Male",
                                      email: "jaiviru@thakur.com")
        
        XCAssertErrorHandler {
            objc_sync_enter(self)
            try storeCoordinator?.db?.insertCustomClassValue(customObject: mockPersonObject)
            objc_sync_exit(self)
        }
        
        XCAssertErrorHandler {
            let objectValue = try storeCoordinator?.db?.getCustomObject(forId: "100")
            guard let personObject = objectValue else {
                XCTAssert(false, "Person object should not b nil.")
                return
            }
            
            XCTAssertEqual(personObject.id, mockPersonObject.id, "Person Id should b same.")
            XCTAssertEqual(personObject.firstName, mockPersonObject.firstName, "Person first name should b same.")
            XCTAssertEqual(personObject.lastName, mockPersonObject.lastName, "Person last name should b same.")
            XCTAssertEqual(personObject.gender, mockPersonObject.gender, "Person gender should b same.")
            XCTAssertEqual(personObject.email, mockPersonObject.email, "Person email should b same.")
        }
    }
    
    
    //Test multiple threading operation
    func testMultiThreadOperations() {
        
        let exp = expectation(description: "multipleTreading")
        
        DispatchQueue.global().asyncAfter(deadline: .now() + 1.0) {
            for i in 0...25 {
                DispatchQueue.global().async {
                    do {
                        let writeResulet = try self.kvStore?.put(key: String(i), value: String(i) as AnyObject)
                        print("*******************************************************")
                        print("WRITE : key : \(String(i)) Result : \(writeResulet)")
                        print("*******************************************************")
                    } catch KVStoreWriteObjectError.dictionaryConversionFailed(let message) {
                        print(message);
                    }
                    catch let error as NSError {
                        print("Error: \(error.localizedDescription)");
                    }
                }
            }
        }
        
        DispatchQueue.global().asyncAfter(deadline: .now() + 1.0) {
            for i in 26...50 {
                DispatchQueue.global().async {
                    do {
                        let writeResulet = try self.kvStore?.put(key: String(i), value: String(i) as AnyObject)
                        print("*******************************************************")
                        print("WRITE : key : \(String(i)) Result : \(writeResulet)")
                        print("*******************************************************")
                    } catch KVStoreWriteObjectError.dictionaryConversionFailed(let message) {
                        print(message);
                    }
                    catch let error as NSError {
                        print("Error: \(error.localizedDescription)");
                    }
                    
                }
            }
        }
        
        let dispatchGroup = DispatchGroup.init()
        dispatchGroup.enter()
        dispatchGroup.wait(timeout: .now() + 9.0)
        dispatchGroup.leave()
        
        dispatchGroup.notify(queue: DispatchQueue.main, execute: {
            for i in 0...50 {
                let valueObject = self.kvStore?.getGenericValue(forKey: String(i))
                
                print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
                print("READ : key : \(String(i)) Value : \(valueObject?.value)")
                print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
                
                XCTAssertNotNil(valueObject?.value, "Value should no b nil")
                XCTAssertEqual(String(i), valueObject!.value as! String, "Values should b same as index(i)")
            }
            exp.fulfill()
        })
        
        
        waitForExpectations(timeout: 10.0, handler: nil)
        
    }
}
